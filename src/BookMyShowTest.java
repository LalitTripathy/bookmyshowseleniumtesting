import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.security.Key;

public class BookMyShowTest {
    static String Selectbrowser;
    static WebDriver ChromeApp;
    static WebDriver firefoxApp;
    static String projectSystemLocation;

    public static void setBrowserConfig(){
        Selectbrowser = "Chrome";
        projectSystemLocation = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", projectSystemLocation + "/lib/chromedriver");
        ChromeApp = new ChromeDriver();
        }
    public static void ToOpenBookMyShowWebsite(){
        setBrowserConfig();
        ChromeApp.get("https://in.bookmyshow.com/");
    }
    public static void ChromewindowManage(){
        ChromeApp.manage().window().maximize();

    }
    public static void handelWebsitePopUp() throws InterruptedException {
        WebElement PopChooseBengaluru =  ChromeApp.findElement(By.xpath("//*[@id=\"modal-root\"]/div/div/div/div[2]/ul/li[3]"));
        PopChooseBengaluru.click();
        Thread.sleep(4000);
        WebElement Personalized = ChromeApp.findElement(By.id("wzrk-confirm"));
        Personalized.click();
        Thread.sleep(4000);
    }
    public static void Scroll() throws InterruptedException {
        Actions act = new Actions(ChromeApp);
        act.sendKeys(Keys.PAGE_DOWN).build().perform();
        Thread.sleep(2000);
        act.sendKeys(Keys.PAGE_UP).build().perform();
    }


        public static void AvoidChromepopUp(){

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-pop-up-blocking");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        ChromeApp =new ChromeDriver(options);
        ChromeApp.get("https://in.bookmyshow.com/explore/home/bengaluru");
        }
public static void MovieSearchBox() throws InterruptedException {
    WebElement searchBox = ChromeApp.findElement(By.xpath("//*[@id=\"1\"]"));
    searchBox.click();
    WebElement searchBoxPopUp = ChromeApp.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[2]/div[2]/div[1]/div/div[2]/div/div/div/input"));
    searchBoxPopUp.sendKeys("Doctor Strange: In The Multiverse Of Madness");
    Thread.sleep(2000);
    searchBoxPopUp.sendKeys(Keys.ENTER);
}
public static void bookTickets() throws InterruptedException {
//        WebElement BookTickets = ChromeApp.findElement(By.xpath("//*[@id=\"page-cta-container\"]/button/div"));
    WebElement BookTickets = ChromeApp.findElement(By.className("cgQNto"));
    BookTickets.click();
//    HindiTickets();
}

    //To book hindi Tickets
//    public static void HindiTickets() throws InterruptedException {
//        WebElement getID = ChromeApp.getClass()
//
//        WebElement Hindi2DTicket = ChromeApp.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div/div[2]/div/div/ul/li[1]/section[2]/div"));
//        Thread.sleep(2000);
//        Hindi2DTicket.click();
//        WebElement bookData = ChromeApp.findElement(By.id())
//    }


    public static void SignInPage(){
        WebElement SignIn = ChromeApp.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/header/div[1]/div/div/div/div[2]/div[2]"));
        SignIn.click();

    }

    public static void main(String[] args) throws InterruptedException {
        ToOpenBookMyShowWebsite();
        ChromewindowManage();
        handelWebsitePopUp();
//      AvoidChromepopUp();
////    SignInPage();
        MovieSearchBox();
        bookTickets();
//        Scroll();


    }
}